#!/bin/bash
if [ $# != 1 ]
then
	echo "need one argumeent"
	echo 'Usage: ./run_putnam filename'
	exit
fi

touch $1.txt
gcc -o run_putnam putnam.c
./run_putnam
