#include "gen_rand.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "proc_mat.h"
double my_max(double* array, int n ){

double M = 1.0*n;
int c, location = 1.0;

 double  maximum;
maximum = array[ 0];

for (c=1; c<=M; c++){
if(array[c] > maximum){
 maximum = array[ c ];
   location = c+1;
  }

}
return maximum;
}

double proc_mat_func(int n){

extern void dgeev_(char *JOBVL, char *JOBVR, int *N, double *A, int *LDA, double *WR, double *WI, double *VL, int *LDVL, double *VR, int *LDVR, double *WORK, int *LWORK, int *INFO);

double M = 1.0*n;

double* A;


A  = (double*) calloc((M+1)*(M+1), sizeof( double ));

 printf("Matrix A is as follows:\n");
      for (int i=0; i<(M+1)*(M+1); i++){
       //int seed = time(NULL);
       //srand(seed);

          A[i] = gen_rand_normal(0.0,1.0);
              printf(" %lf\n", A[i]);
}

double Mx = 0.0;
double Mxx = 0.0;
double Mxy = 0.0;
double Myy = 0.0;
double sumeigenReal = 0.0;
double My = 0.0;
double sumeigenIm = 0.0;
double sumeigenReal2 = 0.0;
double sumeigenRealIm = 0.0;
double sumeigenIm2 = 0.0;
double rhohatmax = 0.0;
double* rhohat =  (double*) calloc((M+1),sizeof(double));


{
int N = M+1;

char JOBVL = 'N';

char JOBVR = 'N';

int LDA = M+1;

double* WR =  (double*) malloc((M+1)*sizeof(double));

double* WI =  (double*) malloc((M+1)*sizeof(double));

double* VL =  (double*) malloc((M+1)*sizeof(double));

int LDVL = M+1;

double* VR =  (double*) malloc((M+1)*sizeof(double));

int LDVR = M+1;

int LWORK = 2*(M+1)*(M+1);

double *WORK = (double*) calloc( (LWORK), sizeof( double ) );

int INFO =0;

double* MAT = (double*) malloc((M+1)*(M+1)*sizeof(double));

memcpy(MAT, A, (M+1) * (M+1) * sizeof(double));

dgeev_(&JOBVL, &JOBVR, &N, &(MAT[0]), &LDA, &(WR[0]),&(WI[0]), &(VL[0]), &LDVL, &(VR[0]), &LDVR,  &(WORK[0]), &LWORK, &INFO);



////////////////PRINTING THE EIGENVALUES////////////////////////////////

printf("The real part of the eigenvalues are as follows:\n");

     for ( int i = 0; i <=M; i++ ){

          printf("\t%lf\n",WR[i]);
}

printf("The imaginary part of the eigenvalues are as follows:\n");

     for ( int i = 0; i <=M; i++ ){

          printf("\t%lf\n",WI[i]);
}
////////////////SOLVING FOR THE SIX MEASUREMENTS/////////////////////////



for (int i = 0; i<=M; i++){

  sumeigenReal += WR[i];
}
Mx = sumeigenReal/(M+1);

for (int i = 0; i<=M; i++){

  sumeigenIm += WI[i];
}
My = sumeigenIm/(M+1);

for (int i = 0; i<=M; i++){

  sumeigenReal2 += WR[i]*WR[i];
}
Mxx = sumeigenReal2/(M+1);


for (int i = 0; i<=M; i++){

  sumeigenRealIm += WR[i]*WI[i];
}
Mxy = sumeigenRealIm/(M+1);

for (int i = 0; i<=M; i++){

  sumeigenIm2 += WI[i]*WI[i];
}
Myy = sumeigenIm2/(M+1);

for(int i=0; i<=M; i++){
rhohat[i] = sqrt(WR[i]*WR[i] + WI[i]*WI[i]);
printf("RHOHATMAX %lf\n", rhohat[i]);
}
rhohatmax = my_max(rhohat,n);


free(MAT);
free(WR);
free(WI);
free(VR);
free(VL);
free(WORK);
}

printf("Mx = %lf\t My = %lf\t Mxx = %lf\t Mxy = %lf\t Myy = %lf rhohatmax = %lf\n" , Mx, My, Mxx, Mxy, Myy, rhohatmax);


FILE* fp;

fp = fopen("solutionfile.txt","a");
fprintf(fp, "%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n", Mx, My, Mxx, Mxy, Myy, rhohatmax);
fclose(fp);

free(A);
   
return 0;

}
