#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>



#define M 19

extern void dgesv_(int *N, int *NRHS, double *A, int *LDA, int *IPIV, double *B, int *LDB, int *INFO);


double F1(double q, double r, double s){return 1;} 
double F2(double q, double r, double s){return q;} 
double F3(double q, double r, double s){return r;} 
double F4(double q, double r, double s){return s;} 
double F5(double q, double r, double s){return q*q;} 
double F6(double q, double r, double s){return r*r;} 
double F7(double q, double r, double s){return s*s;} 
double F8(double q, double r, double s){return q*r;} 
double F9(double q, double r, double s){return q*s;} 
double F10(double q, double r, double s){return r*s;} 
double F11(double q, double r, double s){return q*q*q;} 
double F12(double q, double r, double s){return r*r*r;} 
double F13(double q, double r, double s){return s*s*s;} 
double F14(double q, double r, double s){return q*r*r;} 
double F15(double q, double r, double s){return q*q*r;} 
double F16(double q, double r, double s){return q*s*s;} 
double F17(double q, double r, double s){return q*q*s;} 
double F18(double q, double r, double s){return r*s*s;} 
double F19(double q, double r, double s){return r*r*s;} 
double F20(double q, double r, double s){return q*r*s;} 
 
double (*FP[])(double, double, double) = {F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,F11,F12,F13,F14,F15,F16,F17,F18,F19,F20};



int main(int argc, char** argv)
{
  double* x;
  double* y;
  double* z;
  double* f;
char* fptr = argv[1]; 

 FILE* dfile = fopen(fptr, "r");

//GETTING THE NUMBER OF DATA POINTS FOR x,y,z and f//
int p = 0;
char c;
int n;

while(!feof(dfile))
{
  c = fgetc(dfile);
  if(c == '\n')
  {
   p++;
   n = p;
  }
}
    fclose(dfile); 

//SAVING THE FOUR Columns x,y,z and f//

 FILE* ddfile = fopen(fptr, "r");

 x = (double*) malloc(n * sizeof(double));

 y = (double*) malloc(n * sizeof(double));

 z = (double*) malloc(n * sizeof(double));

 f = (double*) malloc(n * sizeof(double));

for ( int i = 0; i < n; i++ ) 
 {
   fscanf( ddfile, "%lf\t%lf\t%lf\t%lf\n", x+i, y+i, z+i, f+i );
   printf(" ", x+i, y+i, z+i, f+i); 
}
 fclose(ddfile);



//Matrix//
 
 double*     A;
 double*     b;
 double* astar;


 A     = (double*) calloc((M+1)*(M+1), sizeof( double ));
 b     = (double*) calloc((M+1), sizeof( double ));
// astar = (double*) calloc( (M+1), sizeof( double ) );

 double* Fn = (double*) malloc( (M+1) * sizeof( double ) );




for (int i = 0; i<n; i++){ 

 for (int j = 0; j<=M; j++){

  Fn[j] = FP[j](x[i], y[i], z[i]); 
}


 for (int j = 0; j<=M; j++){

  b[j] += f[i]*Fn[j];

}




 for (int k = 0; k<=M; k++){

   for (int l = 0; l<=M; l++){

      A[k*(M+1) + l] += Fn[k]*Fn[l];
}

}

}

//SOLVING THE SYSTEM A*astar=b USING LAPACK//

{
int N = M+1;
int NRHS = 1;
int LDA = M+1;
int* IPIV = (int*) malloc((M+1)*sizeof(int));
int LDB = M+1;
int INFO;
double* MAT = (double*) malloc((M+1)*(M+1)*sizeof(double)); 
double* astar = (double*) malloc((M+1)*sizeof(double));

memcpy( astar, b, (M+1) * sizeof(double) );

memcpy(MAT, A, (M+1) * (M+1) * sizeof(double));
 
dgesv_(&N, &NRHS, &(MAT[0]), &LDA, &(IPIV[0]), &(astar[0]), &LDB, &INFO);


printf("The solution vector astar is as follows:\n");

    for ( int i = 0; i <=M; i++ ){

     printf("\t%lf\n",astar[i]);
}

free(IPIV);

free(MAT);

free(astar);

}

free(A);

free(b);

free(x);

free(y);

free(z);

free(f);

free(Fn);




return 0;

}

